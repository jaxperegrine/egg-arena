extends Control

class_name FightController

var player_character = Character.new()
var ennemy_character = Character.new()

var player_fighter = Fighter.new(player_character)
var ennemy_fighter = Fighter.new(ennemy_character)

func _init():
	player_character.name = "Jax"
	ennemy_character.name = "Chocco"

func _ready():
	($HBoxContainer/VBoxContainer/fighter_stats as FighterStats).fighter = player_fighter
	($HBoxContainer/VBoxContainer3/fighter_stats2 as FighterStats).fighter = ennemy_fighter
	_update_hit_chances()

@onready var story_panel: StoryPanel = %StoryPanel

func add_paragraph(text: String):
	await story_panel.add_text("\n\n" + text)

func _perform_player_action(action: ActionType):
	await story_panel.end_turn()

	var ctx = ActionContext.new(self, player_fighter, ennemy_fighter)
	await action.perform(ctx)
	_update_hit_chances()

	await ctx.wait(1)
	await _perform_enemy_action()
	await ctx.wait(1)

	player_fighter.apply_end_of_turn()
	ennemy_fighter.apply_end_of_turn()
	ctx.update_stats()

	_update_hit_chances()

var _ai := FightingAi.new()

func _perform_enemy_action():
	var ctx = ActionContext.new(self, ennemy_fighter, player_fighter)
	await _ai.perform_action(ctx)

func _update_hit_chances():
	var ctx = ActionContext.new(self, player_fighter, ennemy_fighter)
	_set_hit_chance_text(%AttackHead/TooltipArea, ActionType.AttackHead.new(), ctx)
	_set_hit_chance_text(%AttackChest/TooltipArea, ActionType.AttackChest.new(), ctx)
	_set_hit_chance_text(%AttackNuts/TooltipArea, ActionType.AttackGonads.new(), ctx)

func _set_hit_chance_text(tooltip: TooltipArea, action: ActionType, ctx: ActionContext):
	var chance = action.hit_chance(ctx)

	var color = "red"
	if chance >= 0.75: color = "green"
	elif chance >= 0.50: color = "orange"

	tooltip.contents = "Chance to hit: [color=" + color + "]" + str(roundf(chance * 100)) + "%[/color]"

func _on_attack_head_pressed():
	_perform_player_action(ActionType.AttackHead.new())

func _on_attack_chest_pressed():
	_perform_player_action(ActionType.AttackChest.new())

func _on_attack_nuts_pressed():
	_perform_player_action(ActionType.AttackGonads.new())

func _on_reset_pressed():
	player_fighter.reset()
	ennemy_fighter.reset()
	story_panel.reset()
	_update_hit_chances()

func _on_rest_pressed():
	_perform_player_action(ActionType.Rest.new())
