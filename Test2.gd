extends VBoxContainer

@onready var story_panel := %StoryPanel

func _on_button_pressed():
	$HBoxContainer/Button.text = "Add Text"
	await story_panel.add_text("\n\nLorem Ipsum dolor sit amet!\n\n2!")
	$HBoxContainer/Button.text = "Add Text (done!)"

func _on_button_2_pressed():
	$HBoxContainer/Button2.text = "End Turn"
	await story_panel.end_turn()
	$HBoxContainer/Button2.text = "End Turn (done!)"

func _on_button_3_pressed():
	$HBoxContainer/Button.text = "Add Text"
	$HBoxContainer/Button2.text = "End Turn"
	story_panel.reset()
