extends Node

var PAIN = {
	Fighter.PainStatus.MILD: Effect.new("Mild Pain", "", [
		Effect.Modifier.new(Stat.Type.AGILITY, -10, Effect.Modifier.Type.PERCENTAGE)
	]),
	Fighter.PainStatus.SEVERE: Effect.new("Severe Pain", "", [
		Effect.Modifier.new(Stat.Type.AGILITY, -25, Effect.Modifier.Type.PERCENTAGE)
	]),
	Fighter.PainStatus.EXTREME: Effect.new("Extreme Pain", "", [
		Effect.Modifier.new(Stat.Type.AGILITY, -50, Effect.Modifier.Type.PERCENTAGE)
	]),
	Fighter.PainStatus.AGONY: Effect.new("Agony", "", [
		Effect.Modifier.new(Stat.Type.AGILITY, -100, Effect.Modifier.Type.PERCENTAGE)
	])
}

var STAMINA = {
	Fighter.StaminaStatus.WINDED: Effect.new("Winded", "", [
		Effect.Modifier.new(Stat.Type.STRENGTH, -20, Effect.Modifier.Type.PERCENTAGE)
	]),
	Fighter.StaminaStatus.TIRED: Effect.new("Tired", "", [
		Effect.Modifier.new(Stat.Type.STRENGTH, -40, Effect.Modifier.Type.PERCENTAGE),
		Effect.Modifier.new(Stat.Type.AGILITY, -10, Effect.Modifier.Type.PERCENTAGE),
	]),
	Fighter.StaminaStatus.WORN_OUT: Effect.new("Worn out", "", [
		Effect.Modifier.new(Stat.Type.STRENGTH, -70, Effect.Modifier.Type.PERCENTAGE),
		Effect.Modifier.new(Stat.Type.AGILITY, -25, Effect.Modifier.Type.PERCENTAGE),
	]),
	Fighter.StaminaStatus.EXHAUSTED: Effect.new("Exhausted", "",[
		Effect.Modifier.new(Stat.Type.STRENGTH, -100, Effect.Modifier.Type.PERCENTAGE),
		Effect.Modifier.new(Stat.Type.AGILITY, -50, Effect.Modifier.Type.PERCENTAGE),
	])
}

var CONSCIOUSNESS = {
	Fighter.ConsciousnessStatus.DIZZY: Effect.new("Dizzy", "", [
		Effect.Modifier.new(Stat.Type.INTELLIGENCE, -10, Effect.Modifier.Type.PERCENTAGE)
	]),
	Fighter.ConsciousnessStatus.DAZED: Effect.new("Dazed", "", [
		Effect.Modifier.new(Stat.Type.INTELLIGENCE, -25, Effect.Modifier.Type.PERCENTAGE)
	]),
	Fighter.ConsciousnessStatus.WOOZY: Effect.new("Woozy", "", [
		Effect.Modifier.new(Stat.Type.INTELLIGENCE, -50, Effect.Modifier.Type.PERCENTAGE),
		Effect.Modifier.new(Stat.Type.AGILITY, -25, Effect.Modifier.Type.PERCENTAGE),
	]),
	Fighter.ConsciousnessStatus.UNCONSCIOUS: Effect.new("Unconscious", "", [
		Effect.Modifier.new(Stat.Type.INTELLIGENCE, -100, Effect.Modifier.Type.PERCENTAGE),
		Effect.Modifier.new(Stat.Type.AGILITY, -50, Effect.Modifier.Type.PERCENTAGE),
	])
}
