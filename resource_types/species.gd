extends Resource
class_name Species

@export var name: String = ""
@export var plural_name: String = ""
@export var adjective: String = ""

@export var aliases: Array[String] = []
@export var names: Array[Name] = []
