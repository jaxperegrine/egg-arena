extends Resource
class_name Name

@export var name: String = ""
@export var plural_name: String = ""
@export var adjective: String = ""
