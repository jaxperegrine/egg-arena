extends RefCounted

class_name Fighter

signal stats_changed

enum ConsciousnessStatus { NEUTRAL = 0, DIZZY, DAZED, WOOZY, UNCONSCIOUS }
enum StaminaStatus { NEUTRAL = 0, WINDED, TIRED, WORN_OUT, EXHAUSTED }
enum PainStatus { NEUTRAL = 0, MILD, SEVERE, EXTREME, AGONY }

var consciousness_status: ConsciousnessStatus = ConsciousnessStatus.NEUTRAL:
	get: return consciousness_status
	set(value):
		if consciousness_status == value: return
		consciousness_status = value
		_update_status_effects()
			

var stamina_status: StaminaStatus = StaminaStatus.NEUTRAL:
	get: return stamina_status
	set(value):
		if stamina_status == value: return
		stamina_status = value
		_update_status_effects()

var pain_status: PainStatus = PainStatus.NEUTRAL:
	get: return pain_status
	set(value):
		if pain_status == value: return
		pain_status = value
		_update_status_effects()

var consciousness := 100:
	get: return consciousness
	set(value):
		value = clampi(value, 0, 100)
		if value < consciousness:
			if value <= 0:
				consciousness_status = ConsciousnessStatus.UNCONSCIOUS
			elif value <= 25:
				consciousness_status = ConsciousnessStatus.WOOZY
			elif value <= 50:
				consciousness_status = ConsciousnessStatus.DAZED
			elif value <= 75:
				consciousness_status = ConsciousnessStatus.DIZZY
		elif value > consciousness:
			if value >= 90 and consciousness_status >= ConsciousnessStatus.DIZZY:
				consciousness_status = ConsciousnessStatus.NEUTRAL
			elif value >= 75 and consciousness_status >= ConsciousnessStatus.DAZED:
				consciousness_status = ConsciousnessStatus.DIZZY
			elif value >= 50 and consciousness_status >= ConsciousnessStatus.WOOZY:
				consciousness_status = ConsciousnessStatus.DAZED
			elif value >= 25 and consciousness_status >= ConsciousnessStatus.UNCONSCIOUS:
				consciousness_status = ConsciousnessStatus.WOOZY
		consciousness = value

var stamina := 100:
	get: return stamina
	set(value):
		value = clampi(value, 0, 100)
		if value < stamina:
			if value <= 0:
				stamina_status = StaminaStatus.EXHAUSTED
			elif value <= 25:
				stamina_status = StaminaStatus.WORN_OUT
			elif value <= 50:
				stamina_status = StaminaStatus.TIRED
			elif value <= 75:
				stamina_status = StaminaStatus.WINDED
		elif value > stamina:
			if value >= 90 and stamina_status >= StaminaStatus.WINDED:
				stamina_status = StaminaStatus.NEUTRAL
			elif value >= 75 and stamina_status >= StaminaStatus.TIRED:
				stamina_status = StaminaStatus.WINDED
			elif value >= 50 and stamina_status >= StaminaStatus.WORN_OUT:
				stamina_status = StaminaStatus.TIRED
			elif value >= 25 and stamina_status >= StaminaStatus.EXHAUSTED:
				stamina_status = StaminaStatus.WORN_OUT
		stamina = value

var pain := 0:
	get: return pain
	set(value):
		value = clampi(value, 0, 100)
		if value > pain:
			if value >= 100:
				pain_status = PainStatus.AGONY
			elif value >= 75:
				pain_status = PainStatus.EXTREME
			elif value >= 50:
				pain_status = PainStatus.SEVERE
			elif value >= 25:
				pain_status = PainStatus.MILD
		elif value < pain:
			if value <= 10 and pain_status >= PainStatus.MILD:
				pain_status = PainStatus.NEUTRAL
			elif value <= 25 and pain_status >= PainStatus.SEVERE:
				pain_status = PainStatus.MILD
			elif value <= 50 and pain_status >= PainStatus.SEVERE:
				pain_status = PainStatus.SEVERE
			elif value <= 75 and pain_status >= PainStatus.AGONY:
				pain_status = PainStatus.EXTREME
		pain = value

var left_gonad_health = 100
var right_gonad_health = 100

var agility := Stat.new(Stat.Type.AGILITY, 100)
var intelligence := Stat.new(Stat.Type.INTELLIGENCE, 100)
var strength := Stat.new(Stat.Type.STRENGTH, 100)
var endurance := Stat.new(Stat.Type.ENDURANCE, 100)

var character: Character

func _init(initial_character: Character):
	character = initial_character

func clone_state() -> Fighter:
	var cloned = Fighter.new(character)
	cloned.consciousness = consciousness
	cloned.pain = pain
	cloned.stamina = stamina

	cloned.left_gonad_health = left_gonad_health
	cloned.right_gonad_health = right_gonad_health

	cloned.consciousness_status = consciousness_status
	cloned.stamina_status = stamina_status
	cloned.pain_status = pain_status

	return cloned

func reset():
	consciousness = 100
	stamina = 100
	pain = 0
	left_gonad_health = 100
	right_gonad_health = 100
	stats_changed.emit()

func _update_status_effects():
	var effects: Array[Effect] = []
	if Effects.CONSCIOUSNESS.has(consciousness_status):
		effects.append(Effects.CONSCIOUSNESS[consciousness_status])
	if Effects.STAMINA.has(stamina_status):
		effects.append(Effects.STAMINA[stamina_status])
	if Effects.PAIN.has(pain_status):
		effects.append(Effects.PAIN[pain_status])
	
	agility.set_effects(effects)
	intelligence.set_effects(effects)
	strength.set_effects(effects)
	endurance.set_effects(effects)

## Deals an amount of damage to the gonads, randomly spread between each one.
func damage_gonads(amount: int):
	var factor = randi_range(0, amount)
	var left_damage = factor
	var right_damage = amount - factor
	
	if left_damage > left_gonad_health:
		right_damage += mini(left_damage - left_gonad_health, right_gonad_health)
		left_damage = left_gonad_health
	elif right_damage > right_gonad_health:
		left_damage += mini(right_damage - right_gonad_health, left_gonad_health)
		right_damage = right_gonad_health

	left_gonad_health -= left_damage
	right_gonad_health -= right_damage

func is_immobilized():
	return consciousness_status == ConsciousnessStatus.UNCONSCIOUS or stamina_status == StaminaStatus.EXHAUSTED or pain_status == PainStatus.AGONY

func is_defending():
	if consciousness_status == ConsciousnessStatus.UNCONSCIOUS: return false
	if stamina_status == StaminaStatus.EXHAUSTED: return false
	if pain_status == PainStatus.AGONY: return true

	return false # TODO: defending

func hit_chance_on(other: Fighter) -> float:
	var mean = (self.intelligence.value / float(self.intelligence.value + other.agility.value))
	# Increase effects of the gap
	return 1.5 * mean

func damage_factor_on(other: Fighter) -> float:
	if other.endurance.value <= 0: return 3.0
	return clampf(self.strength.value / float(other.endurance.value), 0, 3.0)

func has_lost():
	return left_gonad_health <= 0 and right_gonad_health <= 0

var has_just_rested := false
func apply_end_of_turn():
	if has_just_rested:
		has_just_rested = false
		return
	if has_lost():
		return

	consciousness += 5
	pain -= 5
