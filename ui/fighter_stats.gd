extends Control

class_name FighterStats

var fighter: Fighter:
	get: return fighter
	set(new_fighter):
		if fighter: fighter.stats_changed.disconnect(stats_changed)
		new_fighter.stats_changed.connect(stats_changed)
		fighter = new_fighter
		fighter_changed()

func _init(with_fighter: Fighter = null):
	if with_fighter: fighter = with_fighter

@onready var _name_label: Label = $VBoxContainer/FighterName

func fighter_changed():
	if not _name_label: return
	if not fighter: return

	_name_label.text = fighter.character.name
	stats_changed()

@onready var consciousness_bar: CircularHealth = %Consiousness
@onready var _consciousness_label := %ConsciousnessLabel

@onready var stamina_bar: CircularHealth = %Stamina
@onready var _stamina_label := %StaminaLabel

@onready var pain_bar: CircularHealth = %Pain
@onready var _pain_label := %PainLabel

@onready var left_gonad_bar: HealthBar = %LeftGonad
@onready var right_gonad_bar: HealthBar = %RightGonad

func stats_changed():
	consciousness_bar.value = fighter.consciousness / 100.0
	%Consiousness/TooltipArea.value = fighter.consciousness
	stamina_bar.value = fighter.stamina / 100.0
	%Stamina/TooltipArea.value = fighter.stamina
	pain_bar.value = fighter.pain / 100.0
	%Pain/TooltipArea.value = fighter.pain

	left_gonad_bar.value = fighter.left_gonad_health / 100.0
	%LeftGonad/TooltipArea.value = fighter.left_gonad_health
	right_gonad_bar.value = fighter.right_gonad_health / 100.0
	%RightGonad/TooltipArea.value = fighter.right_gonad_health

	_set_status_label(fighter.consciousness_status, Effects.CONSCIOUSNESS, _consciousness_label, %ConsciousnessLabel/TooltipArea)
	_set_status_label(fighter.stamina_status, Effects.STAMINA, _stamina_label, %StaminaLabel/TooltipArea)
	_set_status_label(fighter.pain_status, Effects.PAIN, _pain_label, %PainLabel/TooltipArea)

func _ready():
	fighter_changed()

func _set_status_label(value: int, texts: Dictionary, label: Label, tooltip: TooltipArea):
	if not texts.has(value):
		label.text = ""
		tooltip.visible = false
		return

	tooltip.visible = true

	var effect: Effect = texts[value]
	label.text = effect.name
	tooltip.title = effect.name
	tooltip.contents = effect.bbcode()
	tooltip.descrption = effect.description

	var font_color := Color.BLACK

	match value:
		1: font_color = Color.YELLOW
		2: font_color = Color.ORANGE
		3: font_color = Color.RED

	label.add_theme_color_override("font_color", font_color)
	label.add_theme_constant_override("outline_size", 6 if value == 4 else -1)

