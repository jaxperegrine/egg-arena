@tool

extends Control

class_name HealthBar

var previous_value = 1

var current_value = 0
var value_tween: Tween = null

func update_value(v):
	current_value = v
	if current_value > previous_value:
		previous_value = current_value
	queue_redraw()

var current_undercolor: Color = Color.TRANSPARENT
var undercolor_tween: Tween = null

func update_undercolor(c):
	current_undercolor = c
	queue_redraw()

@export_range(0, 1) var value: float = 0:
	get:
		return value
	set(new_value):
		# Prevent nulls from getting around
		new_value = new_value if new_value else 0.0
		if new_value == value: return

		if not (undercolor_tween and undercolor_tween.is_running()):
			previous_value = value if value else 0.0

		new_value = clampf(new_value, 0, 1)
		value = new_value

		if not get_parent():
			current_value = new_value
			return

		# Animate value
		if value_tween:
			value_tween.kill()
		value_tween = create_tween()
		value_tween.tween_method(update_value, current_value, new_value, 1).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_EXPO)

		# Animate damage bar
		if new_value < previous_value:
			if undercolor_tween:
				undercolor_tween.kill()
			undercolor_tween = create_tween()
			var transparent_color = damaged_bar_color
			transparent_color.a = 0
			undercolor_tween.tween_method(update_undercolor, damaged_bar_color, transparent_color, 3.5).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_EXPO)

@export var flipped: bool = false:
	get: return flipped
	set(val): flipped = val; queue_redraw()

@export_group("Bar style")
@export var bar_color_gradient: Gradient = Gradient.new():
	get: return bar_color_gradient
	set(val): bar_color_gradient = val; queue_redraw()

@export var damaged_bar_color: Color = Color.BLUE

@export_group("Background")
@export var enable_background: bool = true:
	get: return enable_background
	set(val): enable_background = val; queue_redraw()

@export var background_color: Color = Color.BLACK:
	get: return background_color
	set(val): background_color = val; queue_redraw()

const PI_HALF = PI / 2
const PI_2 = PI * 2

func _draw():
	if enable_background:
		draw_bar(1, background_color)
	
	var bar_color = bar_color_gradient.sample(current_value)

	if previous_value > current_value:
		draw_bar(previous_value, current_undercolor)
		draw_bar(current_value, bar_color)
	else:
		draw_bar(current_value, bar_color)

func draw_bar(bar_value: float, color: Color):
	var rect = get_rect()
	var bar_size = Vector2(rect.size.x * bar_value, rect.size.y)

	var offset = (rect.size.x - bar_size.x) if flipped else 0
	draw_rect(Rect2(Vector2(offset, 0), bar_size), color)
