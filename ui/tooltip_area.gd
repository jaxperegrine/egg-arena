@tool
extends Control
class_name TooltipArea

@export var title: String = ""

@export var show_value: bool = false
@export var value: int = 0
@export var max_value: int = 0

@export_multiline var contents: String = "":
	get: return contents
	set(val):
		contents = val
		if _tooltip_visible:
			TooltipPanel.display_tooltip(self)

@export_multiline var descrption: String = ""

func _init():
	self.mouse_filter = MOUSE_FILTER_PASS

func _ready():
	mouse_entered.connect(_mouse_entered)
	mouse_exited.connect(_mouse_left)

func _gui_input(event: InputEvent):
	if not _tooltip_visible: return

	if event is InputEventMouseMotion:
		TooltipPanel.move_to((event as InputEventMouseMotion).global_position)

var _tooltip_visible := false
func _mouse_entered():
	if not visible: return
	TooltipPanel.display_tooltip(self)
	if not _tooltip_visible: TooltipPanel.visibility_count += 1
	_tooltip_visible = true

func _mouse_left():
	if _tooltip_visible: TooltipPanel.visibility_count -= 1
	_tooltip_visible = false

func _exit_tree():
	if _tooltip_visible: _mouse_left()
