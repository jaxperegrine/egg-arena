@tool

extends Control

class_name CircularHealth

var previous_value: float = 1.0

var current_value: float = 0.0
var value_tween: Tween = null

func update_value(v):
	current_value = v
	if reversed:
		if current_value < previous_value:
			previous_value = current_value
	else:
		if current_value > previous_value:
			previous_value = current_value
	queue_redraw()

var current_undercolor: Color = Color.TRANSPARENT
var undercolor_tween: Tween = null

func update_undercolor(c):
	current_undercolor = c
	queue_redraw()

@export_range(0, 1) var value: float = 0.0:
	get:
		return value
	set(new_value):
		# Prevent nulls from getting around
		new_value = new_value if new_value else 0.0
		if new_value == value: return

		if not (undercolor_tween and undercolor_tween.is_running()):
			previous_value = value if value else 0.0

		new_value = clampf(new_value, 0, 1)
		value = new_value

		if not get_parent():
			current_value = new_value
			current_undercolor = bar_color
			return

		# Animate value
		if value_tween:
			value_tween.kill()
		value_tween = create_tween()
		value_tween.tween_method(update_value, current_value, new_value, 1).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_EXPO)

		# Animate damage bar
		if reversed:
			if new_value > previous_value:
				if not (undercolor_tween and undercolor_tween.is_running()):
					if undercolor_tween: undercolor_tween.kill()
					undercolor_tween = create_tween()
					undercolor_tween.tween_method(update_undercolor, damaged_bar_color, bar_color, 3.5).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_EXPO)

		else:
			if new_value < previous_value:
				if undercolor_tween:
					undercolor_tween.kill()
				undercolor_tween = create_tween()
				var transparent_color = damaged_bar_color
				transparent_color.a = 0
				undercolor_tween.tween_method(update_undercolor, damaged_bar_color, transparent_color, 3.5).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_EXPO)

@export var reversed: bool = false:
	get: return reversed
	set(val):
		reversed = val
		previous_value = 0 if reversed else 1
		queue_redraw()

@export_group("Bar style")
@export var bar_thickness: int = 5:
	get: return bar_thickness
	set(val): bar_thickness = val; queue_redraw()

@export var bar_color: Color = Color.DARK_GREEN:
	get: return bar_color
	set(val): bar_color = val; current_undercolor = val; queue_redraw()

@export var damaged_bar_color: Color = Color.BLUE

@export_group("Background")
@export var enable_background: bool = true:
	get: return enable_background
	set(val): enable_background = val; queue_redraw()

@export var background_color: Color = Color.BLACK:
	get: return background_color
	set(val): background_color = val; queue_redraw()

const PI_HALF = PI / 2
const PI_2 = PI * 2

func _draw():
	if reversed:
		if enable_background:
			draw_arc_bar(0, 1 - minf(previous_value, current_value), background_color)

		if previous_value < current_value:
			draw_arc_bar(1 - previous_value, 1 - current_value, current_undercolor)
			draw_arc_bar(1 - previous_value, 1, bar_color)
		else:
			draw_arc_bar(1 - current_value, 1, bar_color)
			
	else:
		if enable_background:
			draw_arc_bar(current_value, 1, background_color)

		if previous_value > current_value:
			draw_arc_bar(current_value, previous_value, current_undercolor)

		draw_arc_bar(0, current_value, bar_color)

const CUTOUT = 0.25
const ROTATE = 0.5
func draw_arc_bar(from: float, to: float, color: Color):
	var minAngle = CUTOUT / 2
	var maxAngle = 1 - CUTOUT / 2

	var correctedFrom = from * (maxAngle - minAngle) + minAngle
	var correctedTo = to * (maxAngle - minAngle) + minAngle

	var angleFrom = PI_2 * ((correctedFrom) + ROTATE)
	var angleTo = PI_2 * ((correctedTo) + ROTATE)
	var points = get_circle_arc_points(angleFrom, angleTo)
	draw_colored_polygon(points, color)

const MAX_POINTS_COUNT = 64
func get_circle_arc_points(angleFrom: float, angleTo: float):
	var rect = get_rect()
	var center = rect.size / 2
	var radius = mini(rect.size.x, rect.size.y) / 2.0
	
	var usage_ratio = absf(angleTo - angleFrom) / PI_2
	var points_count = maxi(3, int(ceilf(MAX_POINTS_COUNT * usage_ratio)))

	var points = PackedVector2Array()

	for i in range(points_count + 1):
		var angle_point = angleFrom + i * (angleTo - angleFrom) / points_count - PI_HALF
		points.append(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for i in range(points_count + 1, 0, -1):
		var angle_point = angleFrom + (i - 1) * (angleTo - angleFrom) / points_count - PI_HALF
		points.append(center + Vector2(cos(angle_point), sin(angle_point)) * (radius - bar_thickness))
	
	return points
