extends PanelContainer

class_name StoryPanel

@onready var _history_container: PanelContainer = %HistoryContainer
@onready var _history_toggle_button: Button = %HistoryToggleButton

var _open := false
var _tween: Tween

func _on_history_toggle_button_pressed():
	if _tween: _tween.kill()

	_open = not _open
	
	var destination_anchor = 1 if _open else 0
	var destination_color = Color.WHITE

	if not _open:
		destination_color.a = 0
	
	_history_toggle_button.text = "Hide History" if _open else "Show History"

	if _open:
		_history_container.visible = true

	_tween = create_tween()
	_tween.tween_property(_history_container, "modulate", destination_color, .4).set_trans(Tween.TRANS_EXPO).set_ease(Tween.EASE_OUT)
	_tween.parallel().tween_property(_history_container, "anchor_bottom", destination_anchor, .4).set_trans(Tween.TRANS_EXPO).set_ease(Tween.EASE_OUT)

	if not _open:
		_tween.tween_callback(func(): _history_container.visible = false)

@onready var _active_round_text: RichTextLabel = %ActiveRoundText

func reset():
	if _text_tween:
		_text_tween.kill()
	_active_round_text.clear()
	_history_text.clear()
	_current_round_text = ""

var _current_round_text: String = ""
var _text_tween: Tween
const CPS := 60.0
func add_text(text: String):
	if _end_turn_tween and _end_turn_tween.is_running():
		await _end_turn_tween.finished

	var initialLen = _active_round_text.get_total_character_count()
	if initialLen == 0:
		text = text.strip_edges(true, false)

	if _text_tween and _text_tween.is_running():
		_text_tween.kill()
		initialLen = _active_round_text.visible_characters
	else:
		_active_round_text.visible_characters = initialLen

	_active_round_text.append_text(text)
	_current_round_text += text
	_active_round_text.scroll_to_line(_active_round_text.get_line_count() - 1)
	
	var currentLen = _active_round_text.get_total_character_count()
	var totalDuration = (currentLen - initialLen) / CPS

	_text_tween = create_tween()
	_text_tween.tween_property(_active_round_text, "visible_characters", currentLen, totalDuration)

	await _text_tween.finished

@onready var _history_text: RichTextLabel = %HistoryText

var _end_turn_tween: Tween
func end_turn():
	if _current_round_text == null or _current_round_text.is_empty():
		return

	if _text_tween and _text_tween.is_running():
		await _text_tween.finished

	if _current_round_text:
		_history_text.add_text(_current_round_text)
		_history_text.add_text("\n\n\n")

	_current_round_text = ""

	var destination_color = Color.WHITE
	destination_color.a = 0

	_end_turn_tween = create_tween()
	_end_turn_tween.tween_property(_active_round_scroll_container, "modulate", destination_color, .75).set_trans(Tween.TRANS_EXPO).set_ease(Tween.EASE_OUT)

	await _end_turn_tween.finished

	_active_round_text.clear()
	_active_round_scroll_container.modulate = Color.WHITE

@onready var _active_round_scroll_container: ScrollContainer = %ActiveRoundScrollContainer
func _on_active_round_text_resized():
	await get_tree().process_frame
	if _active_round_text and _active_round_scroll_container:
		_active_round_scroll_container.scroll_vertical = ceil(_active_round_text.size.y)
