extends Panel

class_name TooltipPanelNode

func _ready():
	visible = false

var visibility_count: int = 0:
	get: return visibility_count
	set(value):
		visibility_count = value
		visible = (visibility_count > 0)

const CUSRSOR_SIZE := Vector2(16, 24)
const MARGIN := Vector2(8, 4)

var _last_mouse_position := Vector2(0, 0)
func move_to(new_position: Vector2):
	_last_mouse_position = new_position
	_refresh_position()

func _refresh_position():
	var viewport_rect = get_viewport_rect()
	var end = viewport_rect.end - size - MARGIN
	position = (_last_mouse_position + CUSRSOR_SIZE).clamp(viewport_rect.position, end)

@onready var _container: VBoxContainer = %VBoxContainer
@onready var _title_label: Label = %TitleLabel
@onready var _value_label: Label = %ValueLabel
@onready var _title_container: HBoxContainer = %HBoxContainer
@onready var _content_label: RichTextLabel = %ContentLabel
@onready var _hseparator: HSeparator = %HSeparator
@onready var _description_label: Label = %DescriptionLabel

func display_tooltip(tooltip: TooltipArea):
	_title_label.text = tooltip.title
	_title_label.visible = not tooltip.title.is_empty()

	_value_label.visible = tooltip.show_value
	_value_label.text = str(tooltip.value)
	if tooltip.max_value > 0:
		_value_label.text += " / " + str(tooltip.max_value)

	_title_container.visible = _title_label.visible or _value_label.visible

	_content_label.clear()
	_content_label.append_text(tooltip.contents)
	_content_label.visible = not tooltip.contents.is_empty()

	_description_label.text = tooltip.descrption
	_description_label.visible = not tooltip.descrption.is_empty()

	_hseparator.visible = _description_label.visible
	
const INNER_MARGIN := Vector2(8, 8)
func _on_v_box_container_resized():
	if not _container: return
	size = _container.size + INNER_MARGIN * 2
	_refresh_position()
