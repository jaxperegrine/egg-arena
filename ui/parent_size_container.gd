## A Simple container that fits children to the size of the container's parent.
@tool

extends Container
class_name ParentSizeContainer

@export var margin_top: int = 0:
	get: return margin_top
	set(value): margin_top = value; queue_sort()

@export var margin_left: int = 0:
	get: return margin_left
	set(value): margin_left = value; queue_sort()

@export var margin_right: int = 0:
	get: return margin_right
	set(value): margin_right = value; queue_sort()

@export var margin_bottom: int = 0:
	get: return margin_bottom
	set(value): margin_bottom = value; queue_sort()
	
@export var reference_node: NodePath:
	get: return reference_node
	set(value): reference_node = value; queue_sort()

func _notification(what):
	if what == NOTIFICATION_SORT_CHILDREN:
		if reference_node == null or reference_node.is_empty(): return
		var reference: Control = get_node(reference_node)
		if not reference: return

		var container_relative_position = global_position - reference.global_position 
		var child_position = Vector2(margin_left, margin_top) - container_relative_position
		var child_size = reference.size - Vector2(margin_left + margin_right, margin_top + margin_bottom)
		var child_rect = Rect2(child_position, child_size)

		for c in get_children():
			fit_child_in_rect(c, child_rect)

