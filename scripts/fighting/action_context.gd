extends RefCounted

class_name ActionContext
var player: Fighter
var enemy: Fighter

var initial_player: Fighter
var initial_enemy: Fighter

var _controller: FightController

func _init(t_controller: FightController, t_player: Fighter, t_enemy: Fighter):
	_controller = t_controller

	player = t_player
	enemy = t_enemy

	initial_player = t_player.clone_state()
	initial_enemy = t_enemy.clone_state()

func write_paragraph(text: String):
	await _controller.add_paragraph(text)

func update_stats():
	if player: player.stats_changed.emit()
	if enemy: enemy.stats_changed.emit()

func wait(seconds: float):
	await _controller.create_tween().tween_interval(seconds).finished

func roll(chance: float) -> bool:
	return randf() <= chance
