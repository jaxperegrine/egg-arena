extends RefCounted

class_name ActionType

func is_available(ctx: ActionContext) -> bool:
	return not ctx.player.is_immobilized()

func makes_sense(ctx: ActionContext) -> bool:
	return is_available(ctx)

func hit_chance(ctx: ActionContext) -> float:
	return ctx.player.hit_chance_on(ctx.enemy)

func perform(ctx: ActionContext):
	await ctx.wait(0) # This is just to make warnings stop, this should be overriden.

class AttackHead:
	extends ActionType
	
	func hit_chance(ctx: ActionContext) -> float:
		return clamp(super.hit_chance(ctx) - 0.25, 0.0, 1.0)

	func perform(ctx: ActionContext):
		if not ctx.roll(hit_chance(ctx)):
			await ctx.write_paragraph("%s tried to kick %s in the head, but he missed!" % [ctx.player.character.name, ctx.enemy.character.name] )
			return

		var damage_factor = ctx.player.damage_factor_on(ctx.enemy)
		await ctx.write_paragraph("%s hit %s in the head!" % [ctx.player.character.name, ctx.enemy.character.name])
		ctx.enemy.consciousness -= 25 * damage_factor
		ctx.update_stats()

class AttackChest:
	extends ActionType

	func hit_chance(ctx: ActionContext) -> float:
		return clamp(super.hit_chance(ctx) + 0.25, 0.0, 1.0)

	func perform(ctx: ActionContext):
		if not ctx.roll(hit_chance(ctx)):
			await ctx.write_paragraph("%s tried to kick %s in the chest, but he missed!" % [ctx.player.character.name, ctx.enemy.character.name] )
			return

		var damage_factor = ctx.player.damage_factor_on(ctx.enemy)
		await ctx.write_paragraph("%s punched %s in the chest!" % [ctx.player.character.name, ctx.enemy.character.name])
		ctx.enemy.stamina -= 25 * damage_factor
		ctx.update_stats()

class AttackGonads:
	extends ActionType

	func perform(ctx: ActionContext):
		if ctx.enemy.is_defending():
			await ctx.write_paragraph("%s tried to kick %s in the groin, but couldn't get past his defences!" % [ctx.player.character.name, ctx.enemy.character.name])
			return

		if not ctx.roll(hit_chance(ctx)):
			await ctx.write_paragraph("%s tried to kick %s in the groin, but he missed!" % [ctx.player.character.name, ctx.enemy.character.name] )
			return

		var damage_factor = ctx.player.damage_factor_on(ctx.enemy)
		await ctx.write_paragraph("%s kicked %s in the nuts!" % [ctx.player.character.name, ctx.enemy.character.name])
		ctx.enemy.pain += 25 * damage_factor
		ctx.enemy.damage_gonads(25 * damage_factor)
		if ctx.enemy.left_gonad_health <= 0 and ctx.initial_enemy.left_gonad_health > 0:
			await ctx.write_paragraph("%s's left testicle has popped!!" % ctx.enemy.character.name)
			ctx.enemy.pain = 100
		if ctx.enemy.right_gonad_health <= 0 and ctx.initial_enemy.right_gonad_health > 0:
			await ctx.write_paragraph("%s's right testicle has popped!!" % ctx.enemy.character.name)
			ctx.enemy.pain = 100
		if ctx.enemy.left_gonad_health <= 0 and ctx.enemy.right_gonad_health <= 0:
			await ctx.write_paragraph("%s is castrated!" % ctx.enemy.character.name)
		ctx.update_stats()

class Rest:
	extends ActionType
	
	func is_available(_ctx: ActionContext): return true

	func makes_sense(ctx: ActionContext):
		if not super.makes_sense(ctx): return false
		return ctx.player.consciousness <= 50 or ctx.player.stamina <= 50 or ctx.player.pain >= 50

	func perform(ctx: ActionContext):
		var text = "%s takes some time to rest."
		if ctx.player.consciousness_status == Fighter.ConsciousnessStatus.UNCONSCIOUS:
			text = "%s lays on the ground, unconscious."
		elif ctx.player.pain_status == Fighter.PainStatus.AGONY:
			text = "%s is down on his knees, cupping his testicles in agony."
		elif ctx.player.stamina_status == Fighter.StaminaStatus.EXHAUSTED:
			text = "%s is breathing heavily, too exhausted to make a single move."
		
		await ctx.write_paragraph(text % [ctx.player.character.name])

		var factor = clamp((ctx.player.consciousness + ctx.player.stamina + (100 - ctx.player.pain)) / 300, 0.5, 1.0)
		ctx.player.consciousness += 20 * factor
		ctx.player.stamina += 20 * factor
		ctx.player.pain -= 20 * factor
		ctx.player.has_just_rested = true
		ctx.update_stats()
