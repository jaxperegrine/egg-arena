extends RefCounted

class_name Stat

enum Type {
	AGILITY,
	ENDURANCE,
	INTELLIGENCE,
	STRENGTH,
}

static func name_of(stat_type: Type) -> String:
	match stat_type:
		Type.AGILITY: return "Agility"
		Type.ENDURANCE: return "Endurance"
		Type.INTELLIGENCE: return "Intelligence"
		Type.STRENGTH: return "Strength"
		_: return ""

var type: Type
var base: int

var value: int

func _init(_type: Type, _base: int):
	type = _type
	base = _base
	value = _base

var current_effects: Array[Effect] = []
var current_percentage_modifiers: Array[Effect.Modifier] = []
var current_value_stat_modifiers: Array[Effect.Modifier] = []

var total_value_modifiers: int = 0
var total_percentage_modifiers: int = 0

func set_effects(effects: Array[Effect]):
	current_effects = []
	current_percentage_modifiers = []
	current_value_stat_modifiers = []
	total_value_modifiers = 0
	total_percentage_modifiers = 0

	for e in effects:
		var matches = false
		for se in e.modifiers:
			if not se.stat_type == type: continue
			matches = true

			if se.type == Effect.Modifier.Type.PERCENTAGE:
				total_percentage_modifiers += se.value
				current_percentage_modifiers.append(se)
			else:
				total_value_modifiers += se.value
				current_value_stat_modifiers.append(se)

		if matches:
			current_effects.append(e)

	value = base + total_value_modifiers
	value = round(value * (100 + total_percentage_modifiers) / 100.0)
	value = max(0, value)

