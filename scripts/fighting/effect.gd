extends RefCounted
class_name Effect

class Modifier:
	enum Type {
		PERCENTAGE, # e.g. Agility: +50%
		VALUE, # e.g. Agility: +3
	}

	var stat_type: Stat.Type
	var value: int
	var type: Type

	func _init(_stat_type: Stat.Type, _value: int, _type: Type):
		stat_type = _stat_type
		value = _value
		type = _type
	
	func bbcode() -> String:
		var stat_name = Stat.name_of(stat_type)
		var color = "green" if value > 0 else "red"
		var value_text = ("+" if value > 0 else "") + str(value) + ("%" if type == Type.PERCENTAGE else "")

		return "%s:  [color=%s]%s[/color]\n" % [stat_name, color, value_text]

var name: String = ""
var description: String = ""
var modifiers: Array[Modifier] = []

func _init(_name: String, _description: String, _modifiers: Array[Modifier]):
	name = _name
	description = _description
	modifiers = _modifiers

func bbcode() -> String:
	var text = ""
	for m in modifiers:
		text += m.bbcode()
	return text
