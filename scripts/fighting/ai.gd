extends RefCounted

class_name FightingAi
 
var actions: Array[ActionType] = [
	ActionType.AttackChest.new(),
	ActionType.AttackGonads.new(),
	ActionType.AttackHead.new(),
	ActionType.Rest.new()
]

func perform_action(ctx: ActionContext):
	var available_actions = actions.filter(func (a: ActionType): return a.is_available(ctx))
	var sensible_actions = available_actions.filter(func (a: ActionType): return a.makes_sense(ctx))
	
	var to_roll = sensible_actions if not sensible_actions.is_empty() else available_actions

	var action: ActionType = to_roll[randi() % to_roll.size()]

	await action.perform(ctx)
