extends GutTest

func test_effect_works():
	var agility = Stat.new(Stat.Type.AGILITY, 10)

	var mild_pain_effect = Effect.Modifier.new(Stat.Type.AGILITY, -20, Effect.Modifier.Type.PERCENTAGE)
	var mild_pain = Effect.new("Mild Pain", "", [mild_pain_effect])
	
	agility.set_effects([mild_pain])
	
	assert_eq(agility.value, 8)
