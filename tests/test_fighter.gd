extends GutTest

func _effect(stat: Stat.Type, value: int) -> Array[Effect]:
	return [Effect.new("Test", "", [Effect.Modifier.new(stat, value, Effect.Modifier.Type.VALUE)])]

func test_hit_chance():
	var player = Fighter.new(Character.new())
	var enemy = Fighter.new(Character.new())

	# By default, they both have decent chances
	assert_almost_eq(player.hit_chance_on(enemy), 0.75, 0.0001)

	# Enemy is twice as agile, should avoid attacks easily
	enemy.agility.set_effects(_effect(Stat.Type.AGILITY, 100))
	assert_almost_eq(player.hit_chance_on(enemy), 0.5, 0.0001)

	player.intelligence.set_effects(_effect(Stat.Type.INTELLIGENCE, 100))
	assert_almost_eq(player.hit_chance_on(enemy), 0.75, 0.0001)

	enemy.agility.set_effects([] as Array[Effect])
	assert_almost_eq(player.hit_chance_on(enemy), 1.0, 0.0001)
	
	player.intelligence.set_effects(_effect(Stat.Type.INTELLIGENCE, -50))
	assert_almost_eq(player.hit_chance_on(enemy), 0.5, 0.0001)
	

func test_hit_damage():
	var player = Fighter.new(Character.new())
	var enemy = Fighter.new(Character.new())

	# By default, normal damage
	assert_eq(player.damage_factor_on(enemy), 1.0)

	# Enemy is twice as strong, should make twice as much damage
	player.strength.set_effects(_effect(Stat.Type.STRENGTH, 100))
	assert_eq(player.damage_factor_on(enemy), 2.0)

	enemy.endurance.set_effects(_effect(Stat.Type.ENDURANCE, 100))
	assert_eq(player.damage_factor_on(enemy), 1.0)

	player.strength.set_effects([] as Array[Effect])
	assert_eq(player.damage_factor_on(enemy), 0.5)
	
	player.strength.set_effects(_effect(Stat.Type.STRENGTH, -50))
	assert_eq(player.damage_factor_on(enemy), 0.25)
	
