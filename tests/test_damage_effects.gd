extends GutTest

func test_works():
	var fighter = Fighter.new(Character.new())
	assert_eq(fighter.pain_status, Fighter.PainStatus.NEUTRAL)
	
	fighter.pain += 25
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)
	fighter.pain -= 5
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)

	fighter.pain += 30
	assert_eq(fighter.pain_status, Fighter.PainStatus.SEVERE)
	fighter.pain -= 5
	assert_eq(fighter.pain_status, Fighter.PainStatus.SEVERE)

	fighter.pain += 30
	assert_eq(fighter.pain_status, Fighter.PainStatus.EXTREME)
	fighter.pain -= 5
	assert_eq(fighter.pain_status, Fighter.PainStatus.EXTREME)

	fighter.pain += 30
	assert_eq(fighter.pain_status, Fighter.PainStatus.AGONY)
	fighter.pain -= 5
	assert_eq(fighter.pain_status, Fighter.PainStatus.AGONY)

func test_within_single_effect():
	var fighter = Fighter.new(Character.new())
	assert_eq(fighter.pain_status, Fighter.PainStatus.NEUTRAL)
	
	fighter.pain += 25
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)

	fighter.pain -= 10 #15
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)

	fighter.pain += 15 # 30
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)

	fighter.pain -= 10 # 20
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)

	fighter.pain += 15 # 35
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)

	fighter.pain -= 5 # 30
	assert_eq(fighter.pain_status, Fighter.PainStatus.MILD)
