extends Control

@export var list: Array[Species]

# Called when the node enters the scene tree for the first time.
func _ready():
	set_physics_process(false)

func _on_hit_pressed():
	$CircularHealth.value -= 0.1
	$HealthBar.value -= 0.1
	$HealthBar2.value -= 0.1

func _on_heal_pressed():
	$CircularHealth.value += 0.1
	$HealthBar.value += 0.1
	$HealthBar2.value += 0.1
